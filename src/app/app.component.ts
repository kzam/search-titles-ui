import { Component, OnInit } from '@angular/core';
import { TitlesService } from './shared/services/titles.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'Search Movie Titles';
  movieTitle : string;
  titleFound : any;
  loading : boolean = false;
  foundNoResults : boolean = false;

  constructor(private titlesService : TitlesService) { }

  searchTitle($event) {
    this.movieTitle = $event;
    this.loading = true;
    this.foundNoResults = false;

    this.titlesService.getTitleByName(this.movieTitle)
      .subscribe((data) => {
        this.loading = false;

        if (data.toString().length === 0) {
          this.foundNoResults = true;
        }

        this.titleFound = data;
      });
  }
}
