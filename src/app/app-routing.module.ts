import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TitleModule } from './title/titles.module';
import { AppComponent } from './app.component';
import { TitleDetailsComponent } from './title/components/details/details.component';

const routes: Routes = [
  { path: 'details/:id', component: TitleDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash : true}), TitleModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
