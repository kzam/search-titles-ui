import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TitlesService {
  baseUrl : string;

  constructor(private httpClient: HttpClient) {
    this.baseUrl = 'http://localhost:3000';
   }

  public getTitles() {
      return this.httpClient.get(this.baseUrl + '/titles');
  }

  public getTitleByName(title: string) {
    const params = new HttpParams().set('title', title);
    return this.httpClient.get(this.baseUrl + '/title', {params});
  }

  public getTitleById(id: string) {
    return this.httpClient.get(this.baseUrl + '/title/' + id);
  }
}
