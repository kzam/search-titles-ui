import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent {

  @Output() titleSearched = new EventEmitter<string>();
  title : string;

  search() {
    this.titleSearched.emit(this.title);
  }

}
