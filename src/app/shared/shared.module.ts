import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatInputModule
} from '@angular/material';

import { SearchComponent } from './components/search/search.component';

@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatInputModule,
    FormsModule
  ],
  exports: [
    MatButtonModule,
    MatInputModule,
    SearchComponent,
    CommonModule,
    FormsModule
  ]
})
export class SharedModule { }
