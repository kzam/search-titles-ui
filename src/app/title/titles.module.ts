import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatChipsModule, MatTableModule } from '@angular/material';

import { TitleDetailsComponent } from './components/details/details.component';

@NgModule({
  declarations: [
    TitleDetailsComponent
  ],
  imports: [
    MatChipsModule,
    MatTableModule,
    CommonModule
  ],
  exports : [
    TitleDetailsComponent
  ]
})
export class TitleModule { }
