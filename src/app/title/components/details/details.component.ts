import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TitlesService } from '../../../shared/services/titles.service';

@Component({
  selector: 'app-title-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.less']
})
export class TitleDetailsComponent implements OnInit {
  details : any;
  displayedColumns: string[] = ['AwardCompany', 'Award', 'AwardYear', 'AwardWon'];

  constructor (private route: ActivatedRoute, private titleService: TitlesService) {}

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.titleService.getTitleById(routeParams["id"])
        .subscribe((data) => {
          this.details = data;
        });
    })
  }
}
